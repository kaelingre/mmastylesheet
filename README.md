# My favorite (dark) stylesheet for Mathematica

Installation:
* Copy (or symlink) the file `myTheme.nb` into the folder `$UserBaseDirectory/SystemFiles/FrontEnd/StyleSheets/`
* Copy (or symlink) the file `Package.nb` into the folder `$UserBaseDirectory/SystemFiles/FrontEnd/StyleSheets/`
* Copy (or symlink) the file `frontend.css` into the folder `$UserBaseDirectory/FrontEnd/`
* Evaluate the following commands in MMA to make it the default stylesheet:
```
file=FileNameJoin[{$UserBaseDirectory,"SystemFiles","FrontEnd","StyleSheets","myTheme.nb"}];
SetOptions[$FrontEnd,DefaultStyleDefinitions->file];
```